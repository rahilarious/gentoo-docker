FROM localhost/gentoo/stage3:nomultilib-systemd-merged
ARG MICROARCH_LEVEL
ARG PORTAGE_FLAGS

RUN \
    --mount=type=secret,id=ansible-homelab-vaultpass \
    --mount=type=tmpfs,tmpfs-size=100%,dst=/tmp \
    --mount=type=tmpfs,tmpfs-size=100%,dst=/var/tmp \
    \
    FEATURES='-pid-sandbox -network-sandbox -ipc-sandbox binpkg-ignore-signature' ACCEPT_KEYWORDS='~amd64' CPU_FLAGS_X86="${PORTAGE_FLAGS}" USE='lto jit -iptables nftables pgo -smartcard tofu graphite' emerge -vgk --quiet-build --with-bdeps=y dev-vcs/git 2>/dev/null && \
    FEATURES='-pid-sandbox -network-sandbox -ipc-sandbox binpkg-ignore-signature' ACCEPT_KEYWORDS='~amd64' CPU_FLAGS_X86="${PORTAGE_FLAGS}" USE='lto jit -iptables nftables pgo -smartcard tofu graphite' emerge -vgk --quiet-build --with-bdeps=n -1 app-admin/ansible-core 2>/dev/null && \
    \
    mkdir -v /tmp/ansible-homelab && \
    git clone https://gitlab.com/rahilarious/ansible-homelab.git /tmp/ansible-homelab && \
    cat /run/secrets/ansible-homelab-vaultpass > /tmp/ansible-homelab/.vaultpass && \
    echo '############         Dirty trick to force it as container            #########' && \
    sed -i -e '/virtualization_type/d' /tmp/ansible-homelab/playbooks/set_facts.yml && \
    cd /tmp/ansible-homelab && \
    echo '############         Installing ansible requirements            #########' && \
    ansible-galaxy install -r requirements.yml && \
    \
    echo 'gentoo_base_packages:'			>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - app-editors/mg' 				>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - app-misc/tmux'			        >> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - app-shells/bash-completion' 		>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - app-text/tree' 				>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - sys-apps/lsb-release' 			>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - app-shells/tmux-bash-completion' 		>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - sys-process/htop'				>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo 'portage_features:' 				>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - -ipc-sandbox' 				>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - -network-sandbox' 			>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - -pid-sandbox' 				>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo '  - binpkg-ignore-signature' 			>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo 'portage_local_mirror: ""'			>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo "portage_march: x86-64-${MICROARCH_LEVEL}"	>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo "portage_goamd64: ${MICROARCH_LEVEL}" 		>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    echo "portage_cpu_flags: '${PORTAGE_FLAGS}'" 	>> /tmp/ansible-homelab/host_vars/currenthost.yml && \
    \
    ansible-playbook -l currenthost local.yml && \
    emerge --sync 2>/dev/null && \
    emerge -q1ugkb --with-bdeps=y sys-apps/portage 2>/dev/null && \
    echo '############         Rebuilding whole stage3            #########' && \
    emerge -vgkb --quiet-build --with-bdeps=y -e @world 2>/dev/null && \
    \
    echo '############         Post installation configurations            #########' && \
    echo '############         setting mg as default editor            #########' && \
    eselect editor set mg && \
    echo '############         Cleaning up            #########' && \
    emerge -c 2>/dev/null && \
    rm -rf /root/.ansible /tmp/ansible* /var/tmp/portage && \
    rm /var/log/emerge.log /var/log/portage/elog/summary.log && \
    truncate -s0 /var/log/emerge* /etc/portage/binrepos.conf && \
    echo '###########       CONGRATULATIONS !!! EVERYTHING SUCCESSFUL      #######'



LABEL \
    org.opencontainers.image.description="Gentoo with ~amd64, -march=x86-64-${MICROARCH_LEVEL}, nomultilib-systemd-merged-usr profile and few added utilities" \
    org.opencontainers.image.title='Gentoo for GenZ' \
    org.opencontainers.image.source='https://github.com/rahilarious/gentoo-containers' \
    org.opencontainers.image.url='https://github.com/rahilarious/gentoo-containers' \
    org.opencontainers.image.authors='Rahil Bhimjiani (rahil3108@gmail.com)' \

CMD ["/usr/bin/bash"]
